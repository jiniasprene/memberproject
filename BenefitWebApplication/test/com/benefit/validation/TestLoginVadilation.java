package com.benefit.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import com.benefit.bo.MemberInfo;
import com.benefit.validation.LoginVadilation;

public class TestLoginVadilation {
	@Test
	public void testValidUser() {
		LoginVadilation loginVadilation = new LoginVadilation();
		MemberInfo memberInfo = new MemberInfo();
		memberInfo.setUserName("tom");
		memberInfo.setPassword("tom1");
		//assertEquals(null, errorMessage, "");
		assertNull(loginVadilation.isValidUser("tom", "tom1", memberInfo));
	}
	@Test
	public void testInValidUser() {
		LoginVadilation loginVadilation = new LoginVadilation();
		MemberInfo memberInfo = new MemberInfo();
		memberInfo.setUserName("tom");
		memberInfo.setPassword("tom1");		//assertEquals(null, errorMessage, "");
		//assertNull(loginVadilation.isValidUser("tom", "tom", memberInfo));
		assertEquals("failure - strings are not equal", "Incorrect Password", loginVadilation.isValidUser("tom", "tom", memberInfo));
	}
	@Test
	public void testInValidUsername() {
		LoginVadilation loginVadilation = new LoginVadilation();
		MemberInfo memberInfo = new MemberInfo();
		memberInfo.setUserName("tom");
		memberInfo.setPassword("tom1");
		//assertEquals(null, errorMessage, "");
		//assertNull(loginVadilation.isValidUser("tom", "tom", memberInfo));
		assertEquals("failure - strings are not equal", "Please Enter Password", loginVadilation.isValidUser("tom", "", memberInfo));
	}
	@Test
	public void testInValidPassword() {
		LoginVadilation loginVadilation = new LoginVadilation();
		MemberInfo memberInfo = new MemberInfo();
		memberInfo.setUserName("tom");
		memberInfo.setPassword("tom1");
		//assertEquals(null, errorMessage, "");
		//assertNull(loginVadilation.isValidUser("tom", "tom", memberInfo));
		assertEquals("failure - strings are not equal", "Please Enter User Name", loginVadilation.isValidUser("", "", memberInfo));
	}
	
}
