<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.benefit.bo.ClaimInfo"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Claims Details</title>
<script>
	function validateForm() {
		if (document.myForm.memberid.value == ""
				|| isNaN(document.myForm.memberid.value)) {

			alert("Please provide a memberId in the Integer format");
			document.myForm.memberid.focus();
			return false;
		}
		if (document.myForm.claimid.value == ""
				|| isNaN(document.myForm.claimid.value)) {

			alert("Please provide a claimId in the Integer format.");
			document.myForm.claimid.focus();
			return false;
		}
		if (myForm.startdate.value != '' && !myForm.startdate.value.match(re)) {
			alert("Invalid date format: " + myForm.startdate.value);
			myForm.startdate.focus();
			return false;
		}

		if (document.myForm.physician.value == "") {
			alert("Please provide physician name!");
			document.myForm.physician.focus();
			return false;
		}
		if (document.myForm.status.value == "") {
			alert("Please provide status!");
			document.myForm.status.focus();
			return false;
		}

	}
</script>
<style>
.button {
	display: inline-block;
	padding: 10px 14px;
	font-size: 15px;
	cursor: pointer;
	text-align: center;
	text-decoration: none;
	outline: none;
	color: #fff;
	background-color: #4CAF50;
	border: none;
	border-radius: 10px;
	box-shadow: 0 9px #999;
}

.button:hover {
	background-color: #3e8e41
}

.button:active {
	background-color: #3e8e41;
	box-shadow: 0 5px #666;
	transform: translateY(4px);
}

input[type=text] {
	width: 265px;
	box-sizing: border-box;
	border: 2px solid #ccc;
	border-radius: 4px;
	font-size: 25px;
	background-color: white;
	background-position: 10px 10px;
	background-repeat: no-repeat;
	padding: 12px 20px 12px 40px;
	transition: width 0.4s ease-in-out;
}
</style>

</head>
<body>
	<div id="claimdiv" class="claimcontent">
		<form name="myForm" action="/BenefitWebApplication/SubmitClaimServlet"
			onsubmit="return validateForm()" method="post">
			<h1 align="center">Claim Details</h1>
			<br>
			<table cellspacing="5" cellpadding="4" border="1">

				<tr>
					<td align="right"><label for="memberid">Member Id:</label></td>
					<td><input type="text" id="memberid" name="memberid"><br>
						<br></td>
				<tr>
					<td align="right"><label for="claimid">Claim Id:</label></td>
					<td><input type="text" id="claimid" name="claimid"><br>
						<br></td>
				<tr>
					<td align="right"><label for="servicedate">Service
							Date:</label></td>
					<td><input type="text" id="servicedate" name="servicedate"><br>
						<br></td>
				<tr>
					<td align="right"><label for="physician">Physician :</label></td>
					<td><input type="text" id="physician" name="physician"><br>
						<br></td>
				<tr>
					<td align="right"><label for="status">Status:</label></td>
					<td><input type="text" id="status" name="status"><br>
						<br></td>
			</table>
			<button class="button" type="submit">Submit Claim</button>
		</form>
	</div>
</body>
</html>