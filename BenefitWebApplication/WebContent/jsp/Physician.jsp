<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.benefit.bo.PhysicianInfo"%>
<%@ page import="java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Physician Information</title>
<script>
	function validateForm() {
		let x = document.forms["myForm"]["zipcode"].value;
		if (isNaN(x)) {
			alert("Enter valid zipcode");
			return false;
		}
	}
</script>
<style>
.physiciandiv input[type=text] {
	padding: 6px;
	margin-top: 8px;
	font-size: 17px;
	border: none;
}

.physiciandiv a, .physiciandiv input[type=text] {
	float: none;
	display: block;
	text-align: left;
	width: 100%;
	margin: 0;
	padding: 14px;
}

.physiciandiv input[type=text] {
	border: 1px solid #ccc;
}

.button {
	display: inline-block;
	padding: 10px 14px;
	font-size: 15px;
	cursor: pointer;
	text-align: center;
	text-decoration: none;
	outline: none;
	color: #fff;
	background-color: #4CAF50;
	border: none;
	border-radius: 10px;
	box-shadow: 0 9px #999;
}

.button:hover {
	background-color: #3e8e41
}

.button:active {
	background-color: #3e8e41;
	box-shadow: 0 5px #666;
	transform: translateY(4px);
}

input[type=text] {
	width: 130px;
	box-sizing: border-box;
	border: 2px solid #ccc;
	border-radius: 4px;
	font-size: 18px;
	background-color: white;
	background-position: 10px 10px;
	background-repeat: no-repeat;
	padding: 12px 20px 12px 40px;
	transition: width 0.4s ease-in-out;
}
</style>
</head>
<body>


	<div id="physiciandiv" class="tabcontent">
		<form name="myForm" action="/BenefitWebApplication/PhysicianServlet"
			onsubmit="return validateForm()" method="post">
			<h1>Physician Details</h1>
			<input type="text" placeholder="Search.." name="zipcode">
			<button class="button" type="submit">Submit</button>
		</form>


		<%
			List<PhysicianInfo> physicianInfoList = (List<PhysicianInfo>) session.getAttribute("physicianInfoList");
		%>
		<%
			if (physicianInfoList != null) {
			for (PhysicianInfo physicianInfo : physicianInfoList) {
		%>
		<b>Physician Details</b> <br> Physician Id:
		<%=physicianInfo.getPhysicianId()%><br> Physician Name:
		<%=physicianInfo.getPhysicianName()%><br> Specialist:
		<%=physicianInfo.getSpecilist()%>
		<br> Address:
		<%=physicianInfo.getAddress()%>
		<br> ZipCode:
		<%=physicianInfo.getZipcode()%>
		<br> Care Facility Name:
		<%=physicianInfo.getHealthcareFelicityName()%>
		<br> Contact No:
		<%=physicianInfo.getContactNo()%>
		<br>
		<%
			}
		}
		%>
	</div>
</body>
</html>