<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Member Login</title>
<script>
function validateForm() {
  let x = document.forms["myForm"]["username"].value;
  if (x == "") {
    alert("Name must be filled out");
    return false;
  }
}
</script>
</head>
<body>
	<form name="myForm" action="/BenefitWebApplication/LoginServlet" method="post" onsubmit="return validateForm()">
		User Name: <input type="text" name="username" size="20"  /><br />
		<br /> Password: <input type="text" name="password" size="20"  /><br />
		<input type="submit" value="Login" /><br />
	</form>
</body>
</html>