<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.benefit.bo.InsuranceInfo"%>
<%@ page import="java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insurance Shop</title>
<script>
	function validateForm() {
		let x = document.forms["myForm"]["zipcode"].value;
		if (isNaN(x)) {
			alert("Enter valid zipcode");
			return false;
		}
	}
</script>
<style>
.button {
	display: inline-block;
	padding: 10px 14px;
	font-size: 15px;
	cursor: pointer;
	text-align: center;
	text-decoration: none;
	outline: none;
	color: #fff;
	background-color: #4CAF50;
	border: none;
	border-radius: 10px;
	box-shadow: 0 9px #999;
}

.button:hover {
	background-color: #3e8e41
}

.button:active {
	background-color: #3e8e41;
	box-shadow: 0 5px #666;
	transform: translateY(4px);
}

input[type=text] {
	width: 130px;
	box-sizing: border-box;
	border: 2px solid #ccc;
	border-radius: 4px;
	font-size: 18px;
	background-color: white;
	background-position: 10px 10px;
	background-repeat: no-repeat;
	padding: 12px 20px 12px 40px;
	transition: width 0.4s ease-in-out;
}
</style>
</head>
<body>
	<div id="insurancediv" class="tabcontent">
		<form name="myForm" action="/BenefitWebApplication/InsuranceServlet"
			onsubmit="return validateForm()" method="post">
			<h1>
				Insurance<br> Find the health insurance option that meets your
				needs.
			</h1>
			<input type="text" placeholder="Search.." name="zipcode">
			<button class="button" type="submit">Submit</button>
		</form>

		<%
			List<InsuranceInfo> insuranceInfoList = (List<InsuranceInfo>) session.getAttribute("insuranceInfoList");
		%>
		<%
			if (insuranceInfoList != null) {
			for (InsuranceInfo insuranceInfo : insuranceInfoList) {
		%>
		<b>Insurance Covered Here</b> <br> State:
		<%=insuranceInfo.getState()%><br> Insurance Name:
		<%=insuranceInfo.getInsuraneName()%><br>

		<%
			}
		}
		%>

	</div>
</body>
</html>