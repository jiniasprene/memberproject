<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.benefit.bo.ClaimInfo"%>
<%@ page import="com.benefit.bo.BenefitInfo"%>
<%@ page import="java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page</title>
</head>
<style>
<
body>body {
	font-family: Arial;
}

.tab {
	overflow: hidden;
	border: 1px solid #ccc;
	background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
	background-color: inherit;
	float: left;
	border: none;
	outline: none;
	cursor: pointer;
	padding: 14px 16px;
	transition: 0.3s;
	font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
	background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
	background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
	display: none;
	padding: 6px 12px;
	border: 1px solid #ccc;
	border-top: none;
}

table, td, th {
	border: 1px solid black;
}

table {
	border-collapse: collapse;
	width: 100%;
}

th {
	height: 70px;
}
</style>
<script>
	var counter = 0;
	function openWindow() {
		window
				.open(
						'http://localhost:8080/BenefitWebApplication/jsp/Physician.jsp',
						'mywindow' + counter, 'width=400,height=350');
		counter++;
	}
</script>
<script>
	var counter = 0;
	function openInsuranceWindow() {
		window
				.open(
						'http://localhost:8080/BenefitWebApplication/jsp/FindInsurace.jsp',
						'mywindow' + counter, 'width=300,height=250');
		counter++;
	}
</script>
<script>
	var counter = 0;
	function openClaimWindow() {
		window
				.open(
						'http://localhost:8080/BenefitWebApplication/jsp/ClaimSubmit.jsp',
						'mywindow' + counter, 'width=300,height=250');
		counter++;
	}
</script>
</head>
<body>

	<form>
		<b>Welcome <%
			out.println(session.getAttribute("name"));
		%> !!!!
		</b>

	</form>

	<h2>Insurance Details</h2>
	<div class="innnertab">

		<input type="button" value="Find Insurance" id="button"
			onclick="openInsuranceWindow()" />
		<button class="innertablinks">Customer Service</button>

		<button class="innertablinks">Health and Wellness</button>
		<input type="button" value="Find a Doctor" id="button"
			onclick="openWindow()" /> <a
			href="http://localhost:8080/BenefitWebApplication/jsp/MemberLogin.jsp">Logout</a>

	</div>
	<p>Click on the buttons inside the tabbed menu:</p>
	<div class="tab">
		<button class="tablinks" onclick="openCity(event, 'London')">Benefits</button>
		<button class="tablinks" onclick="openCity(event, 'Paris')">Claims</button>
		<button class="tablinks" onclick="openCity(event, 'News')">News</button>
		<button class="tablinks" onclick="openCity(event, 'Tokyo')">About
			us</button>
		<div class="search-container">
			<input style="float: right" type="text" placeholder="Search..">
		</div>
	</div>

	<div id="London" class="tabcontent">
		<h3>Benefits</h3>
		<form>
			<b>Details of Benefits <%
				out.println(session.getAttribute("name"));
			%></b>
		</form>
		<%
			BenefitInfo benefitInfo = (BenefitInfo) session.getAttribute("benefitInfo");
		%>
		Plan Id:
		<%=benefitInfo.getPlanId()%><br> Plan Name:
		<%=benefitInfo.getPlanName()%><br> Deductible:
		<%=benefitInfo.getDeductible()%><br> Co-insurance:
		<%=benefitInfo.getCoInsurance()%><br> Co-insurance Max:
		<%=benefitInfo.getCoinsuranceMax()%><br> Out of Pocket Maximum:
		<%=benefitInfo.getOutofpocketMaximum()%>
	</div>

	<div id="Paris" class="tabcontent">
		<h3>Claim</h3>
		<table>
			<tr>
				<th>ClaimId</th>
				<th>MemberId</th>
				<th>Service Date</th>
				<th>Physician</th>
				<th>Status</th>
			</tr>
			<%
				List<ClaimInfo> claimInfoList = (List<ClaimInfo>) session.getAttribute("claimInfo");
			for (int i = 0; i < claimInfoList.size(); i++) {
			%>
			<tr>
				<td><%=claimInfoList.get(i).getClaimId()%></td>
				<td><%=claimInfoList.get(i).getMemberId()%></td>
				<td><%=claimInfoList.get(i).getServicedate()%></td>
				<td><%=claimInfoList.get(i).getPhysician()%></td>
				<td><%=claimInfoList.get(i).getStatus()%></td>
			</tr>
			<%
				}
			%>
		</table>
		<br>
		<br> <input type="button" value="Submit Claim" id="button"
			onclick="openClaimWindow()" />

	</div>
	<div id="News" class="tabcontent">
		<h2>Recent news releases</h2>
		<ul id="listid">
			<li><a href='http://www.google.com' onclick='return check()'>Todays
					News</a></li>
			<li><a href='http://example.com' onclick='return check()'>Example</a></li>
			<li><a href='http://www.google.com' onclick='return check()'>Todays
					News</a></li>
			<li><a href="https://www.w3schools.com/html/"
				title="Go to W3Schools HTML section">Visit our HTML Tutorial</a></li>
		</ul>
	</div>

	<div id="Tokyo" class="tabcontent">
		<h3>About Us</h3>
		<p>Best Insurance Ever.</p>
	</div>

	<script>
		function openCity(evt, cityName) {
			var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(
						" active", "");
			}
			document.getElementById(cityName).style.display = "block";
			evt.currentTarget.className += " active";
		}
	</script>

</body>
</html>