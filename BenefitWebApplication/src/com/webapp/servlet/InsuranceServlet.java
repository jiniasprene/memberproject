package com.webapp.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.benefit.bo.InsuranceInfo;
import com.benefit.dao.DataAccess;

/**
 * Servlet implementation class InsuranceServlet
 */
@WebServlet("/InsuranceServlet")
public class InsuranceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static final Logger log = LogManager.getLogger(PhysicianServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public InsuranceServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		log.debug("Entring");
		String zipCode = request.getParameter("zipcode");
		DataAccess dataAccess = new DataAccess();
		List<InsuranceInfo> InsuranceServletList = dataAccess.getInsuranceInfo(Integer.parseInt(zipCode));
		HttpSession session = request.getSession();
		session.setAttribute("insuranceInfoList", InsuranceServletList);
		response.sendRedirect("jsp/FindInsurace.jsp");
		log.debug("Exiting");
	}

}
