package com.webapp.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.benefit.bo.ClaimInfo;
import com.benefit.dao.DataAccess;
@WebServlet("/SubmitClaimServlet")
public class SubmitClaimServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static final Logger log = LogManager.getLogger(SubmitClaimServlet.class);
	
	// Database credentials
	

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SubmitClaimServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("finally")
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		log.info("Entring");
		PrintWriter pw = response.getWriter();
		String memberId = request.getParameter("memberid");
		String claimId = request.getParameter("claimid");
		String serviceDate = request.getParameter("servicedate");
		String physician = request.getParameter("physician");
		String status = request.getParameter("status");
		DataAccess dataAccess = new DataAccess();
		boolean responseStatus = dataAccess.addClaimInfo(Integer.valueOf(memberId), Integer.valueOf(claimId), serviceDate, physician, status);
		
		String msg = "";
		if (responseStatus == true) {
			msg = "Record has been inserted";
			pw.println("<font size='6' color=blue>" + msg + "</font>");

		} else {
			msg = "failed to insert the data";
			pw.println("<font size='6' color=blue>" + msg + "</font>");
		}
		log.debug("Exiting");
	}

}
