package com.webapp.servlet;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import com.benefit.bo.BenefitInfo;
import com.benefit.bo.ClaimInfo;
import com.benefit.bo.MemberInfo;
import com.benefit.dao.DataAccess;
import com.benefit.validation.LoginVadilation;

/**
 * Servlet implementation class MemberLoginServlet
 */
@WebServlet("/MemberLoginServlet")
public class MemberLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static final Logger log = LogManager.getLogger(MemberLoginServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MemberLoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		log.debug("Entring");
		// log.info("Entring");
		// log.warn("Entring");
		// log.error("Entring");
		// log.fatal("Entring");
		// TODO Auto-generated method stub
		doGet(request, response);
		String userName = request.getParameter("username");
		String password = request.getParameter("password");
		log.info("Use name is: " + userName);
		LoginVadilation loginVadilation = new LoginVadilation();
		DataAccess dataAccess = new DataAccess();
		MemberInfo memberInfo = dataAccess.getMemberLogin(userName);
		List<ClaimInfo> claimInfoList = dataAccess.getClaimInfo(memberInfo.getMemberId());
		BenefitInfo benefitInfo = dataAccess.getBenefitInfo(memberInfo.getMemberId());
		String errorMsg = loginVadilation.isValidUser(userName, password, memberInfo);
		// String errorMsg = dataAccess.getMemberLogin();
		if (errorMsg == null) {
			log.info("Entring into Home Page " + userName);
			// String name = loginVadilation.getMemberInfo(userName).getFirstName() + " " +
			// loginVadilation.getMemberInfo(userName).getLastname();
			HttpSession session = request.getSession();
			// session.setAttribute("userName", userName);
			session.setAttribute("name", memberInfo.getFirstName() + " " + memberInfo.getLastName());
			session.setAttribute("claimInfo", claimInfoList);
			session.setAttribute("benefitInfo", benefitInfo);
			// session.setAttribute("claimInfo", );
			request.setAttribute("claimInfo", claimInfoList);
			response.sendRedirect("jsp/HomePage.jsp");
			// req.include(request, response);
			session.getAttribute("name");
		} else {
			log.error("Entered UserName and Password are incorrect");
			request.setAttribute("username", userName);
			request.setAttribute("errorMessage", "");
			log.error("Wrong User Name Password");
			// RequestDispatcher req = request.getRequestDispatcher("jsp/MemberLogin.jsp");
			// req.forward(request, response);
			response.sendRedirect("jsp/MemberLogin.jsp");
		}
		log.debug("Exiting");
	}

}
