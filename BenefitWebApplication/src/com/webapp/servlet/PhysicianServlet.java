package com.webapp.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.benefit.bo.PhysicianInfo;
import com.benefit.dao.DataAccess;

/**
 * Servlet implementation class PhysicianServlet
 */
@WebServlet("/PhysicianServlet")
public class PhysicianServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static final Logger log = LogManager.getLogger(PhysicianServlet.class);
 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PhysicianServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		log.debug("Entring");
		String zipCode = request.getParameter("zipcode");
		Client client = ClientBuilder.newClient();
		/*
		 * WebTarget target = client.target(
		 * "http://localhost:8081/memberservice/services/physicianservice?zip=234567");
		 * Response responseWS = target.request().get(); System.out.println(responseWS);
		 */
		DataAccess dataAccess = new DataAccess();
		List<PhysicianInfo> physicianInfoList = dataAccess.getPhysicianInfo(Integer.parseInt(zipCode));
		HttpSession session = request.getSession();
		session.setAttribute("physicianInfoList", physicianInfoList);
		response.sendRedirect("jsp/Physician.jsp");
		log.debug("Exiting");
	}

}
