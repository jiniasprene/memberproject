package com.benefit.validation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.benefit.bo.MemberInfo;
import com.webapp.servlet.MemberLoginServlet;

public class LoginVadilation {
	static final Logger log = LogManager.getLogger(MemberLoginServlet.class);
	/*public MemberInfo isValidUser(String username, String password,MemberInfo userName ) {
		String errorMsg = null;
		Map<String, MemberInfo> userMap = new HashMap<>();
		 Adding elements to userMap 
		userMap.put("tom", new MemberInfo("Tom", "Raj", null, "", "", "", "tom1"));
		userMap.put("rahul", new MemberInfo("Tahul", "Taa", null, "", "", "", "rahul1"));
		userMap.put("singh", new MemberInfo("Singh", "Sin", null, "", "", "", "singh1"));
		userMap.put("ajeet", new MemberInfo("Ajeet", "Ajj", null, "", "", "", "ajeet1"));
		userMap.put("asrt", new MemberInfo("Asrt", "Assr", null, "", "", "", "asrt1"));

		if (userNameI.isEmpty()) {
			errorMsg = "Please Enter User Name";
		} else if (password.isEmpty()) {
			errorMsg = "Please Enter Password";
		}else if (userName.getUserName() == null) {
			errorMsg = "User not found";
		} else if (!userName.getUserName().equals(password)) {
			errorMsg = "Incorrect Password";
		}return userName;
		/*else if (userMap.get(userName) == null) {
			errorMsg = "User not found";
		} else if (!userMap.get(userName).getPassword().equals(password)) {
			errorMsg = "Incorrect Password";
		}
		
	}*/
	
	public String isValidUser(String submittedUserName, String submittedPassword, MemberInfo memberInfo) {
		log.info("Validating the user");
		String errorMsg = null;
		if (submittedUserName.isEmpty()) {
			errorMsg = "Please Enter User Name";
		} else if (submittedPassword.isEmpty()) {
			errorMsg = "Please Enter Password";
		}else if (memberInfo.getUserName() == null) {
			errorMsg = "User not found";
		} else if (!memberInfo.getPassword().equals(submittedPassword)) {
			errorMsg = "Incorrect Password";
			log.error("Incorrect Password");
		}
		log.debug("Exiting");
		return errorMsg;
		}
}
