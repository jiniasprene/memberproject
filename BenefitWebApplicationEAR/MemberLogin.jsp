<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/page_style.css" />
<meta charset="ISO-8859-1">
<title>Member Login</title>
<script>
function validateForm() {
  let x = document.forms["myForm"]["username"].value;
  let y = document.forms["myForm"]["password"].value;
  if (x == "" || y=="") {
    alert("Name and Password must be filled out");
    return false;
  }
}
</script>
</head>
<body>
	<div class="topnav">
		<a href="#">Providers</a> <a href="#">News</a> <a href="#">AboutUs</a>
		<a href="#">ContactUs</a>
	</div>

		<div class="row">
		<div class="column">
			<h2>Hi Let have a talk</h2>
			<p>Get to Know as.Get to answer from us.</p>
			<button class="quesbutton">Question?</button>
		</div>

		<div class="column1">
			<form  name = "myForm" action="/SampleWebApplication/MemberLoginServlet" onsubmit="return validateForm()" method="post">
				<b>LoginForm</b><br>
				User Name: <input type="text" name="username"
					value='<%if (request.getAttribute("username") != null) {
					out.print(request.getAttribute("username"));
					}%>' /><br />
				<br /> Password: <input type="text" name="password" /><br /> <input
					type="submit" value="Login" /><br />
				<% if (request.getAttribute("errorMessage") != null) {
					out.print(request.getAttribute("errorMessage"));
				}%>
			</form>
		</div>
	</div>
	<div class="footer">
		<p>If you have any question or comments about this site please
			notify</p>
	</div>
</body>
</html>