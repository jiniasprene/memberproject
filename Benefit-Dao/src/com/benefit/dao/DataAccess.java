package com.benefit.dao;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.benefit.bo.BenefitInfo;
import com.benefit.bo.ClaimInfo;
import com.benefit.bo.InsuranceInfo;
import com.benefit.bo.MemberInfo;
import com.benefit.bo.PhysicianInfo;

public class DataAccess {

	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost/member";
	static final Logger log = LogManager.getLogger(DataAccess.class);
	// Database credentials
	static final String USER = "root";
	static final String PASS = "password";

	@SuppressWarnings("finally")
	public MemberInfo getMemberLogin(String submittedUserName) {
		log.info("Entring");
		MemberInfo memberInfo = new MemberInfo();
		Connection conn = null;
		Statement stmt = null;

		try {
			// Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");
			// Open a connection
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			// Execute SQL query
			stmt = conn.createStatement();

			String sql;
			log.info("Fetching the LoginUserName " + submittedUserName);
			sql = "SELECT user_name,password,first_name,last_name,member_id FROM loginmemberinfo WHERE user_name = '"
					+ submittedUserName + "'";
			ResultSet rs = stmt.executeQuery(sql);

			// Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				memberInfo.setUserName(rs.getString("user_name"));
				memberInfo.setPassword(rs.getString("password"));
				memberInfo.setFirstName(rs.getString("first_name"));
				memberInfo.setLastName(rs.getString("last_name"));
				memberInfo.setMemberId(rs.getInt("member_id"));
			}
			// Clean-up environment
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			log.error("Error in SQLException", se);
			// se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			log.error("Error in Exception ", e);
			// e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException se2) {
				log.error("Errors", se2);
			} // nothing we can do
			log.debug("Exiting");
			return memberInfo;

		}
	}

	@SuppressWarnings("finally")
	public List<ClaimInfo> getClaimInfo(Integer memberId) {
		log.info("Entring");
		List<ClaimInfo> claimInfoList = new ArrayList<>();

		// String login = null;
		Connection conn = null;
		Statement stmt = null;

		try {
			// Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");
			// Open a connection
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			// Execute SQL query
			stmt = conn.createStatement();
			String sql;
			log.info("Fetching the ClaimInformation of " + memberId);
			sql = "SELECT * FROM claim_info WHERE member_id = " + memberId;
			ResultSet rs = stmt.executeQuery(sql);

			// Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				log.info("Retrieve by column name");
				ClaimInfo claimInfo = new ClaimInfo();
				claimInfo.setMemberId(rs.getString("member_id"));
				claimInfo.setClaimId(rs.getString("claim_id"));
				claimInfo.setPhysician(rs.getString("physician"));
				claimInfo.setServicedate(rs.getDate("service_date"));
				claimInfo.setStatus(rs.getString("status"));
				claimInfoList.add(claimInfo);
			}

			// Clean-up environment
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			log.error("Errors in SQLException", se);
		} catch (Exception e) {
			// Handle errors for Class.forName
			log.error("Errors Exception", e);
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException se2) {
				log.error("SQLException", se2);
			} // nothing we can do
			log.debug("Exiting");
			return claimInfoList;
		}
	}

	@SuppressWarnings("finally")
	public BenefitInfo getBenefitInfo(Integer memberId) {
		log.info("Entring");
		// List<BenefitInfo> benefitInfoList = new ArrayList<>();
		BenefitInfo benefitInfo = new BenefitInfo();
		// String login = null;
		Connection conn = null;
		Statement stmt = null;

		try {
			// Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");
			// Open a connection
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			// Execute SQL query
			stmt = conn.createStatement();
			String sql;
			log.info("Fetching the ClaimInformation of " + memberId);
			sql = "select b.plan_id,b.plan_name,b.deductible,b.co_insurance,b.coinsurance_max,b.outofpocket_maximum from member.memberinfo mf, "
					+ "member.benefits b where mf.plan_id = b.plan_id and mf.memberid = " + memberId;
			ResultSet rs = stmt.executeQuery(sql);

			// Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				log.info("Retrieve by column name");
				// BenefitInfo benefitInfo = new BenefitInfo();
				benefitInfo.setPlanId(rs.getInt("plan_id"));
				benefitInfo.setPlanName(rs.getString("plan_name"));
				benefitInfo.setDeductible(rs.getFloat("deductible"));
				benefitInfo.setCoInsurance(rs.getInt("co_insurance"));
				benefitInfo.setCoinsuranceMax(rs.getFloat("coinsurance_max"));
				benefitInfo.setOutofpocketMaximum(rs.getFloat("outofpocket_maximum"));
				// benefitInfoList.add(benefitInfo);
			}

			// Clean-up environment
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			log.error("Errors in SQLException", se);
		} catch (Exception e) {
			// Handle errors for Class.forName
			log.error("Errors Exception", e);
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException se2) {
				log.error("SQLException", se2);
			} // nothing we can do
			log.debug("Exiting");
			return benefitInfo;
		}
	}

	@SuppressWarnings("finally")
	public List<PhysicianInfo> getPhysicianInfo(Integer zipcode) {
		log.info("Entring");
		List<PhysicianInfo> physicianInfoList = new ArrayList<>();
		Connection conn = null;
		Statement stmt = null;

		try {
			// Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");
			// Open a connection
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			// Execute SQL query
			stmt = conn.createStatement();
			String sql;
			log.info("Fetching the PhysicianInformation of " + zipcode);
			sql = "SELECT * FROM member.physician_info WHERE zipcode=" + zipcode;
			ResultSet rs = stmt.executeQuery(sql);

			// Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				log.info("Retrieve by column name");
				PhysicianInfo physicianInfo = new PhysicianInfo();
				physicianInfo.setPhysicianId(rs.getInt("physician_id"));
				physicianInfo.setPhysicianName(rs.getString("physician_name"));
				physicianInfo.setSpecilist(rs.getString("specilist"));
				physicianInfo.setAddress(rs.getString("address"));
				physicianInfo.setZipcode(rs.getInt("zipcode"));
				physicianInfo.setHealthcareFelicityName(rs.getString("healthcare_felicityname"));
				physicianInfo.setContactNo(rs.getInt("contact_no"));
				physicianInfoList.add(physicianInfo);
			}

			// Clean-up environment
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			log.error("Errors in SQLException", se);
		} catch (Exception e) {
			// Handle errors for Class.forName
			log.error("Errors Exception", e);
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException se2) {
				log.error("SQLException", se2);
			} // nothing we can do
			log.debug("Exiting");
			return physicianInfoList;
		}
	}

	@SuppressWarnings("finally")
	public List<InsuranceInfo> getInsuranceInfo(Integer zipcode) {
		log.info("Entring");
		List<InsuranceInfo> insuranceInfoList = new ArrayList<>();
		Connection conn = null;
		Statement stmt = null;

		try {
			// Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");
			// Open a connection
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			// Execute SQL query
			stmt = conn.createStatement();
			String sql;
			sql = "SELECT state,insurance_name FROM member.insurance_info WHERE zip_code=" + zipcode;
			ResultSet rs = stmt.executeQuery(sql);

			// Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				log.info("Retrieve by column name");
				InsuranceInfo insuranceInfo = new InsuranceInfo();
				insuranceInfo.setInsuraneName(rs.getString("insurance_name"));
				insuranceInfo.setState(rs.getString("state"));
				insuranceInfoList.add(insuranceInfo);
			}

			// Clean-up environment
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			log.error("Errors in SQLException", se);
		} catch (Exception e) {
			// Handle errors for Class.forName
			log.error("Errors Exception", e);
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException se2) {
				log.error("SQLException", se2);
			} // nothing we can do
			log.debug("Exiting");
			return insuranceInfoList;
		}
	}

	public Boolean addClaimInfo(Integer memberId, Integer claimId, String serviceDate, String physician,
			String status) {
		log.info("Entring");
		Connection conn = null;
		boolean responseStatus = false;
		try {

			// Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");
			// Open a connection
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			// Execute SQL query
			PreparedStatement pstmt = conn.prepareStatement(
					"insert into member.claim_info (member_id,claim_id,service_date,physician,status) values(?,?,?,?,?)");
			//ResultSet rs = pstmt.executeQuery();
			// For the first parameter,
			// get the data using request object
			// sets the data to stmt pointer
			pstmt.setInt(1, memberId);
			pstmt.setInt(2, claimId);
			pstmt.setString(3, serviceDate);
			pstmt.setString(4, physician);
			pstmt.setString(5, status);
			// Execute the insert command using executeUpdate()
			// to make changes in database
			if (pstmt.executeUpdate() != 0) {
				responseStatus = true;
			}
			 pstmt.close();
					// Clean-up environment
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			log.error("Errors in SQLException", se);
		} catch (Exception e) {
			// Handle errors for Class.forName
			log.error("Errors Exception", e);
		} finally {
			// finally block used to close resources
			try {

				if (conn != null) {
					conn.close();
				}
			} catch (SQLException se2) {
				log.error("SQLException", se2);
			} // nothing we can do
			log.debug("Exiting");
		}
		return responseStatus;
	}
}
